import { Article } from './article';
import { ItemPanier } from './item-panier';

describe('ItemPanier', () => {
  it('should create an instance', () => {
    const article: Article = new Article(54, "Banjo");
    expect(new ItemPanier(article)).toBeTruthy();
  });
});
