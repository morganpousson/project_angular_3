import { TypeArticle } from "./type-article";

export class Article {
  id: number;
  nom: string;

  prix?: number;
  image?: string;

  types?: TypeArticle[];

  constructor(rId: number, rNom: string,) {
    this.id = rId;
    this.nom = rNom;
  }
}
