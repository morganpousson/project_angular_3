import { Article } from "./article";

export class ItemPanier {
  article: Article;
  nbarticles: number = 0;

  constructor(rArticle: Article) {
    this.article = rArticle;
  }

  /**
   * compte ajout article
   */
  addArticle() {
    this.nbarticles++;
  }

  /**
   * decompte ajout article
   */
   removeArticle() {
    this.nbarticles = Math.max(--this.nbarticles, 0);
  }
}
