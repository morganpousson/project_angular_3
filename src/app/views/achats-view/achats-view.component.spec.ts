import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AchatsViewComponent } from './achats-view.component';

describe('AchatsViewComponent', () => {
  let component: AchatsViewComponent;
  let fixture: ComponentFixture<AchatsViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [AchatsViewComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(AchatsViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
